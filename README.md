# Competencies Visualization

Simple HTML visualization of competencies (competency model) in form of wheel of balance (wheel of life) that uses D3.js library.
All sources are embedded within single HTML file for easier distribution.
The sample contains competencies that I take care for software engineers in my teams but you may use your own data (categories).

Note that there are two files, competencies.html is in English and kompetence.html in Czech.

Some parts of the visualization are taken from Peter Cook's [d3-radialbar](https://github.com/prcweb/d3-radialbar)
